interface User {
    gitlabId: string
}

interface UsersMap {
    [key: string]: User
}

export const USERS: UsersMap = {
    '00000-01': {
        gitlabId: '2989550'
    },
    '00000-02': {
        gitlabId: '4724920'
    },
    '00000-03': {
        gitlabId: '3829046'
    },
    '00000-04': {
        gitlabId: '6658883'
    },
    '00000-05': {
        gitlabId: '6658883'
    },
    '00000-06': {
        gitlabId: '3829046'
    },
    '00000-07': {
        gitlabId: '4724920'
    },
    '00000-08': {
        gitlabId: '6658883'
    },
    '00000-09': {
        gitlabId: '3829046'
    },
    '00000-10': {
        gitlabId: '4724920'
    }
};
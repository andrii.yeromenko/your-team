import React from 'react';

import './loading.css';

export const Loading = () => (
    <div className="loading">Loading</div>
);
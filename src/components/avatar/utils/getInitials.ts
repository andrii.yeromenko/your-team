export const getInitials = (fullName: string) => fullName
    .split(' ')
    .map(name => name.charAt(0))
    .reduce((acc, curr) => acc + curr);
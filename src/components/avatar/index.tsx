import React from 'react';
import {getInitials} from "./utils/getInitials";

import './avatar.css';

interface Props {
    fullName: string
    url?: string
}

export const Avatar = ({fullName, url}: Props) => {
    const initials = getInitials(fullName);
    return url
        ? (<img className="avatar" src={url} alt={initials}/>)
        : (<span>{initials}</span>);
};
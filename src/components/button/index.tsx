import React from 'react';
import classNames from 'classnames';
import {Link} from "react-router-dom";

import {IconLookup} from '@fortawesome/fontawesome-svg-core';
import './button.css';
import {Icon} from "../icon";

interface Props {
    title: string,
    url?: string,
    external?: boolean,
    disabled?: boolean,
    isLink?: boolean,
    icon?: IconLookup,
}

export const Button = (props: Props) => {
    const {
        title,
        url,
        disabled,
        external,
        isLink,
        icon
    } = props;

    const buttonClasses = classNames('button', {
        'button_disabled': disabled
    });

    const linkClasses = classNames('button_link', {
        'button_link-disabled': disabled
    });

    const classes = isLink ? linkClasses : buttonClasses;
    const body = (
        <>
            {icon && <span className="button__icon"><Icon icon={icon}/></span>}
            <span>{title}</span>
        </>
    );

    return url && !disabled
        ? external
            ? (<a className={classes} href={url} target="_blank" rel="noreferrer">{body}</a>)
            : (<Link to={url} className={classes}>{body}</Link>)
        : (<span className={classes}>{body}</span>);
};

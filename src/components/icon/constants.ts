import {faLinkedin, faGitlab} from '@fortawesome/free-brands-svg-icons';
import {faUser} from '@fortawesome/free-regular-svg-icons';
import {faGlobe, faChevronCircleLeft, faChevronCircleRight} from '@fortawesome/free-solid-svg-icons';

import {IconLookup} from '@fortawesome/fontawesome-svg-core';

export const LINKEDIN = 'LINKEDIN';
export const GITLAB = 'GITLAB';
export const USER = 'USER';
export const WEBSITE = 'WEBSITE';
export const CHEVRON_CIRCLE_LEFT = 'CHEVRON_CIRCLE_LEFT';
export const CHEVRON_CIRCLE_RIGHT = 'CHEVRON_CIRCLE_RIGHT';

export const ICONS = {
    LINKEDIN,
    GITLAB,
    USER,
    WEBSITE,
    CHEVRON_CIRCLE_LEFT,
    CHEVRON_CIRCLE_RIGHT
};

interface IconsMap {
    [key: string]: IconLookup
}

export const ICONS_MAP: IconsMap = {
    [LINKEDIN]: faLinkedin,
    [GITLAB]: faGitlab,
    [USER]: faUser,
    [WEBSITE]: faGlobe,
    [CHEVRON_CIRCLE_LEFT]: faChevronCircleLeft,
    [CHEVRON_CIRCLE_RIGHT]: faChevronCircleRight
};
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import {IconLookup, SizeProp} from '@fortawesome/fontawesome-svg-core';

interface Props {
    icon: IconLookup,
    size?: SizeProp
    color?: string
}

export const Icon =({icon, size, color}: Props) => (
    <FontAwesomeIcon icon={icon} size={size} color={color}/>
);
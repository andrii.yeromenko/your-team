import React, {ReactElement} from 'react';

import './branch.css';

interface Props {
    data: Array<ReactElement>
    horizontalDirection: boolean
}

export const Branch = ({data, horizontalDirection}: Props) => {
    const branchData = data[0];
    const nextBranch = data.slice(1);

    return horizontalDirection
        ? (
            <div className="branch">
                <section className="branch__item branch__item_row">
                    {branchData}
                </section>
                {!!nextBranch.length && <Branch data={nextBranch} horizontalDirection={false}/>}
            </div>
        ) : (
            <div className="branch branch_column">
                <section className="branch__item branch__item_column">
                    {branchData}
                </section>
                {!!nextBranch.length && <Branch data={nextBranch} horizontalDirection={true}/>}
            </div>
        );
};
import React, {ReactElement} from 'react';
import {Branch} from './branch';

import './fibonacci-tree.css';

interface Props {
    data: Array<ReactElement>
}

export const FibonacciTree = ({data}: Props) => {

    return (
        <div className="fibonacci-tree">
            <Branch data={data} horizontalDirection={true}/>
        </div>
    );
};
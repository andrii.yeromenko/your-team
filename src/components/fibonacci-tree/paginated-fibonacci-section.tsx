import React, {ReactElement, useMemo} from 'react';
import {FibonacciTree} from './fibonacci-tree';
import {PrevButton, NextButton} from './buttons';

interface Props {
    content: Array<ReactElement>,
    page: number,
    totalPages: number
    onPaginationChange: (page: number) => void
}

export const FibonacciListSection = (props: Props) => {
    const {content, page, totalPages, onPaginationChange} = props;

    const paginationButtons = useMemo(() => {
        let buttons: Array<ReactElement> = [];

        if (page > 1) {
            buttons = [...buttons, <PrevButton page={page} onClick={() => onPaginationChange(page - 1)}/>];
        }

        if (page < totalPages - 1) {
            buttons = [...buttons, <NextButton page={page} onClick={() => onPaginationChange(page + 1)}/>];
        }

        return buttons;
    }, [page, totalPages, onPaginationChange]);

    return (
        <FibonacciTree data={[
            ...content,
            ...paginationButtons
        ]}/>
    );
};
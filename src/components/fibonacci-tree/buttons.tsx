import React from 'react';
import {Icon} from '../icon';
import {ICONS, ICONS_MAP} from '../icon/constants';

import './fibonacci-tree.css';

interface Props {
    page: number,
    onClick: () => void
}


export const PrevButton = ({page, onClick}: Props) => (
    <div className="fibonacci-tree__button" onClick={onClick}>
        <Icon icon={ICONS_MAP[ICONS.CHEVRON_CIRCLE_LEFT]} size="3x"/>
        {page - 1}
    </div>
);

export const NextButton = ({page, onClick}: Props) => (
    <div className="fibonacci-tree__button" onClick={onClick}>
        <Icon icon={ICONS_MAP[ICONS.CHEVRON_CIRCLE_RIGHT]} size="3x"/>
        {page + 1}
    </div>
);
import {ReactElement, useCallback, useMemo, useState} from "react";

interface Page {
    pageContent: Array<ReactElement>,
    page: number,
    totalPages: number,
    onPaginationChange: (page: number) => void
}

interface Props {
    children: (page: Page) => ReactElement
    content: Array<string>
    mapMethod: (id: string, page: number) => ReactElement
}

const PAGE_SIZE = 4;

export const PaginatedList = ({children, content, mapMethod}: Props) => {
    const [page, setPage] = useState<number>(1);

    const pageContent = useMemo(
        () => content
            .filter((item, index) => index >= (page -1) * PAGE_SIZE && index < page * PAGE_SIZE)
            .map(userId => mapMethod(userId, page)),
        [page, content, mapMethod]);

    const onPaginationChange = useCallback((page: number) => {
        setPage(page);
    }, []);

    const totalPages = useMemo(
        () => content.length / PAGE_SIZE + +(content.length % PAGE_SIZE > 0),
        [content]);

    return children({page, pageContent, totalPages, onPaginationChange});
};
import {combineReducers} from "@reduxjs/toolkit";
import dashboardSlice from '../modules/dashboard/services/dashboardSlice';
import profileSlice from '../modules/profile/services/profileSlice';

export const rootReducer = combineReducers({
    dashboard: dashboardSlice,
    profile: profileSlice
});


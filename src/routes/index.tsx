import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import {Header} from "../modules/header";
import {Footer} from "../modules/footer";
import {ProfileDetails} from "../modules/profile";
import {Dashboard} from "../modules/dashboard";

export const Routes = () => (
    <Router>
        <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', height: '100vh'}}>
            <Header/>
            <Switch>
                <Route exact={true} path="/"><Dashboard/></Route>
                <Route path="/profile/:profileId"><ProfileDetails/></Route>
            </Switch>
            <Footer/>
        </div>
    </Router>
);

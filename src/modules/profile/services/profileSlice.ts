import {createSlice} from '@reduxjs/toolkit';
import {clearProfileUsers, getProfileUser} from "./actions";

import {GitlabUserDetails} from "../../../common-types";

export interface DashboardState {
    loading: boolean,
    error: string,
    user: GitlabUserDetails | null
}

const initialState = {
    loading: false,
    error: '',
    user: null,
} as DashboardState;


const profileSlice = createSlice({
    name: 'dashboard',
    initialState,
    reducers: {},
    extraReducers:(builder) => {
        builder.addCase(clearProfileUsers.type, state => {
            state.user = null
        });
        builder.addCase(getProfileUser.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(getProfileUser.fulfilled, (state, action) => {
            state.loading = false;
            const {data} = action.payload;
            state.user = data;
        });
        builder.addCase(getProfileUser.rejected, (state, action) => {
            state.loading = false;
            state.error = action.error.message || '';
        });
    }
});

export default profileSlice.reducer;
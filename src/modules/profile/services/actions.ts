import {createAction, createAsyncThunk} from "@reduxjs/toolkit";
import {USERS} from "../../../constants";
import {RootState} from "../../../store";
import {getProfileUserSelector} from "./selectors";

interface Meta {
    id: string
}

export const getProfileUser = createAsyncThunk<
    any,
    Meta,
    {
        state: RootState
    }
    >(
    'users/profile/getGitlabUser',
    async (meta, thunkAPI) => {
        const {id} = meta;
        const {user} = getProfileUserSelector(thunkAPI.getState());

        if (user) {
            return
        }

        return fetch(`https://gitlab.com/api/v4/users/${USERS[id].gitlabId}`)
            .then(response => response.json())
            .then(json => ({data: json}));
    }
);

export const clearProfileUsers = createAction('users/profile/clear');
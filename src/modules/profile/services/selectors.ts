import {RootState} from "../../../store";


export const getProfileUserSelector = (state: RootState) => {
    return {user: state.profile.user, error: state.profile.error};
};
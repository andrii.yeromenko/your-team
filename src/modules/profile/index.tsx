import React, {useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {useSelector, useDispatch} from "react-redux";

import {Loading} from "../../components/loading";
import {ProfileDetailsView} from "./views/profile-details.view";
import {getProfileUserSelector} from "./services/selectors";
import {clearProfileUsers, getProfileUser} from "./services/actions";
import {RootState} from "../../store";

export interface ProfileDetailsRouteParams {
    profileId: string
}

export const ProfileDetails = () => {
    const dispatch = useDispatch();
    const {profileId} = useParams<ProfileDetailsRouteParams>();
    const {user, error} = useSelector((state: RootState) => getProfileUserSelector(state));

    useEffect(() => {
        return () => {
            dispatch(clearProfileUsers());
        }
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        dispatch(getProfileUser({id: profileId}));
    }, [profileId]); // eslint-disable-line react-hooks/exhaustive-deps

    return !user
        ? (<Loading/>)
        : error
            ? (<h2>error</h2>)
            : (user && <ProfileDetailsView details={user}/>);
};
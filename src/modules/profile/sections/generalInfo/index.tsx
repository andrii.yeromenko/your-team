import {Icon} from "../../../../components/icon";
import {ICONS, ICONS_MAP} from "../../../../components/icon/constants";
import {Button} from "../../../../components/button";
import {useSelector} from "react-redux";
import {RootState} from "../../../../store";
import {getProfileUserSelector} from "../../services/selectors";

export const GeneralInfo = () => {
    const {user, error} = useSelector((state: RootState) => getProfileUserSelector(state));

    return !error && user ? (
        <section style={{display: "flex", flexDirection: 'column'}}>
            <h3>General Info</h3>
            <span style={{display: 'flex', alignItems: 'center'}}>
                <Icon icon={ICONS_MAP[ICONS.GITLAB]}/>
                <p style={{paddingLeft: '5px'}}>{user?.username}</p>
            </span>

            <p>Social links</p>
            <Button
                title="gitlab"
                isLink={true}
                external={true}
                icon={ICONS_MAP[ICONS.GITLAB]}
                url={user?.web_url}/>
            {user?.website_url &&
                <Button
                    title="website"
                    isLink={true}
                    external={true}
                    icon={ICONS_MAP[ICONS.WEBSITE]}
                    url={user?.website_url}/>
            }
            {user?.linkedin &&
            <Button
                title="linkedin"
                isLink={true}
                external={true}
                icon={ICONS_MAP[ICONS.LINKEDIN]}
                url={user?.linkedin}/>
            }
        </section>
    ) : null;
};
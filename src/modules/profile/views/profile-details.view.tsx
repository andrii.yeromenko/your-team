import React from 'react';

import {ProfileHeader} from "../components/profile-header";
import {GeneralInfo} from "../sections/generalInfo";

import {GitlabUserDetails} from "../../../common-types";

import './profile-details-view.css';

interface Props {
    details: GitlabUserDetails
}

const SECTIONS = [
    GeneralInfo
];

export const ProfileDetailsView = ({details}: Props) => {
    const {name, avatar_url, id} = details;

    return (
        <section className="profile">
            <ProfileHeader userName={name} gitlabId={id} avatarUrl={avatar_url}/>
            <section>
                {SECTIONS.map((Section, i) => (<Section key={i}/>))}
            </section>
        </section>
    );
};

import React from 'react';
import {Avatar} from "../../../../components/avatar";

import './profile-header.css';

interface Props {
    userName: string,
    avatarUrl?: string,
    gitlabId: string
}

export const ProfileHeader = ({userName, avatarUrl, gitlabId}: Props) => {
    return (
        <header className="profile-header">
                <span className="profile-header__user-info">
                    <Avatar fullName={userName} url={avatarUrl}/>
                    <h2 className="profile-header__user-name">{userName}</h2>
                </span>
            <p className="profile-header__gitlab-id">{`GitLab ID: ${gitlabId}`}</p>
        </header>
    );
};
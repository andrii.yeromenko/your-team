import React from 'react';
import {Button} from "../../components/button";

import './header.css';

export const Header = () => (
    <header className="header">
        <h2 className="header__title">Your</h2>
        <Button url="/" title="Team"/>
    </header>
);
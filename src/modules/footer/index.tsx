import {Button} from "../../components/button";

import './footer.css';
import {ICONS, ICONS_MAP} from "../../components/icon/constants";

export const Footer = () => (
    <footer className="footer">
        <p className="footer__copyright">Andrii Yeromenko 2021 ©</p>
        <ul className="footer__social-links">
            <li>
                <Button
                    title="Gitlab"
                    isLink={true}
                    external={true}
                    url="https://gitlab.com/andrii.yeromenko"
                    icon={ICONS_MAP[ICONS.GITLAB]}/>
            </li>
            <li>
                <Button
                    title="LinkedIn"
                    isLink={true}
                    external={true}
                    url="https://www.linkedin.com/in/andrii-yeromenko-46aa58193"
                    icon={ICONS_MAP[ICONS.LINKEDIN]}/>
            </li>
        </ul>
    </footer>
);
import {RootState} from "../../../store";


export const getDashboardUserSelector = (state: RootState, page: number, id: string) => {
    return state.dashboard.users[`${page}-${id}`];
};
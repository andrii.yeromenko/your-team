import {createSlice} from '@reduxjs/toolkit';
import {clearDashboardUsers, getDashboardUser} from "./actions";

import {GitlabUserDetails} from "../../../common-types";

interface UserList {
    [key: string]: GitlabUserDetails;
}

export interface DashboardState {
    loading: boolean,
    error: string,
    users: UserList
}

const initialState = {
    loading: false,
    error: '',
    users: {}
} as DashboardState;


const dashboardSlice = createSlice({
    name: 'dashboard',
    initialState,
    reducers: {},
    extraReducers:(builder) => {
        builder.addCase(clearDashboardUsers.type, state => {
            state.users = {}
        });
        builder.addCase(getDashboardUser.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(getDashboardUser.fulfilled, (state, action) => {
            state.loading = false;
            const {page, id, data} = action.payload;
            state.users = {...state.users, [`${page}-${id}`]: data};
        });
        builder.addCase(getDashboardUser.rejected, (state, action) => {
            state.loading = false;
            state.error = action.error.message || '';
        });
    }
});

export default dashboardSlice.reducer;
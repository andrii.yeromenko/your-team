import {createAction, createAsyncThunk} from "@reduxjs/toolkit";
import {USERS} from "../../../constants";
import {RootState} from "../../../store";
import {getDashboardUserSelector} from "./selectors";

interface Meta {
    page: number,
    id: string
}

export const getDashboardUser = createAsyncThunk<
    any,
    Meta,
    {
        state: RootState
    }
    >(
    'users/getGitlabUser',
    async (meta, thunkAPI) => {
        const {page, id} = meta;
        const existedUser = getDashboardUserSelector(thunkAPI.getState(), page, id);

        if (existedUser) {
            return
        }

        return fetch(`https://gitlab.com/api/v4/users/${USERS[id].gitlabId}`)
            .then(response => response.json())
            .then(json => ({page, id, data: json}));
    }
);

export const clearDashboardUsers = createAction('users/clear');
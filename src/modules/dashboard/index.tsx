import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";

import {UserLink} from "./user-link/user-link";
import {PaginatedList} from '../../components/paginated-list';
import {FibonacciListSection} from "../../components/fibonacci-tree/paginated-fibonacci-section";
import {USERS} from '../../constants';

import './dashboard.css';
import {clearDashboardUsers} from "./services/actions";

export const Dashboard = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        return () => {
            dispatch(clearDashboardUsers);
        }
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <section className="dashboard__user-list">
            <h2>Dashboard</h2>
            <PaginatedList content={Object.keys(USERS)} mapMethod={(userId, page) => (<UserLink {...{userId, page}}/>)}>
                {({pageContent, page, totalPages, onPaginationChange}) => (
                    <FibonacciListSection
                        content={pageContent}
                        page={page}
                        totalPages={totalPages}
                        onPaginationChange={onPaginationChange}/>
                )}
            </PaginatedList>
        </section>
    );
};
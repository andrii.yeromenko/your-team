import React from 'react';
import {UserLinkView} from "./user-link.view";

import {useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {RootState} from "../../../store";
import {getDashboardUser} from "../services/actions";
import {getDashboardUserSelector} from "../services/selectors";

interface Props {
    userId: string,
    page: number
}

export const UserLink = ({userId, page}: Props) => {
    const dispatch = useDispatch();
    const user = useSelector((state: RootState) => getDashboardUserSelector(state, page, userId));

    useEffect(() => {
        if (!user) {
            dispatch(getDashboardUser({page, id: userId}));
        }
    }, [user, page, userId]); // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <>
            {user && <UserLinkView {...{userId, details: user}}/>}
        </>
    );
};
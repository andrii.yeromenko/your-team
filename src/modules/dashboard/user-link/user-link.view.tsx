import React from "react";
import {Avatar} from "../../../components/avatar";
import {Link} from "react-router-dom";

import {GitlabUserDetails} from "../../../common-types";

import './user-link.css';

interface Props {
    userId: string,
    details: GitlabUserDetails
}

export const UserLinkView = ({userId, details}: Props) => {
    return (
        <Link to={`/profile/${userId}`} style={{textDecoration: 'none'}}>
            <span className="user__link">
                <Avatar fullName={details?.name} url={details?.avatar_url}/>
                <h4>{details?.name}</h4>
            </span>
        </Link>
    );
};
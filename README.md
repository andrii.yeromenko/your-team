#Hosted Application
[yourteam-6cad7.web.app](https://yourteam-6cad7.web.app/)

# Getting Started with Create React App (TypeScript Template)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
```shell script
npx create-react-app workshop-app-ts --template typescript  
```

## TypeScript troubleshooting
You can update [TypeScipt Pagckage](https://www.npmjs.com/package/typescript) and [React Types](https://www.npmjs.com/package/@types/react) by a command
```shell script
npm i typescript @types/react
```

## React Router Dom
[React Router Dom](https://www.npmjs.com/package/react-router-dom) and [types](https://www.npmjs.com/package/@types/react-router-dom) install command
```shell script
npm i react-router-dom @types/react-router-dom
```

## ClassNames: 
[classNames](https://www.npmjs.com/package/classnames) util installation command
```shell script
npm i classnames @types/classnames
```

## FontAwesome
You also need to install font-awesome core components and icons libraries
```shell script
npm i @fortawesome/react-fontawesome @fortawesome/fontawesome-svg-core

npm i @fortawesome/free-solid-svg-icons
npm i @fortawesome/free-regular-svg-icons
npm i @fortawesome/free-brands-svg-icons
```

# Redux
You can find Redux Toolkit documentation [here](https://redux-toolkit.js.org/) 
```shell script
npm i react-redux
npm i @reduxjs/toolkit
```

# CI/CD

You can login to [Firebase](https://firebase.google.com/) and get a LOGIN_TOKEN by a command.
```shell script
firebase login:ci --no-localhost 
```

You need to specify CI/CD variable in Settings -> CI/CD section of the repository page.
The same variable key must be used for a deploy job in gitlab-ci.yml
```yaml
    - firebase deploy --token "${FIREBASE_LOGIN_TOKEN}" --project project-id
```


### Usefull JS/React tutorial playlists:
[Сложный JavaScript простым языком](https://www.youtube.com/playlist?list=PLqKQF2ojwm3l4oPjsB9chrJmlhZ-zOzWT)

[Введение в React Hooks](https://www.youtube.com/playlist?list=PLqKQF2ojwm3n6YO3BDSQIg35GGKn_ImFD)

[TypeScript](https://www.youtube.com/playlist?list=PLqKQF2ojwm3nW-cQeSER79xdpK3vL5c-g)


# SPD-University
[SPD-University](https://spd-ukraine.com/uk/university/K) web-site